import Project.DrawingPanel;
import java.awt.*;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class ProjectileMotion{

    private static int WIDTH = 10;
    private static int HEIGHT = 10;
    private static int XPOS = 500;
    private static int YPOS = 500;
    private static double SECONDS = 1;
    private static double GRAVITY = 9.8;

    public static void main(String[] args) throws InterruptedException{
        GetValues();
        
        System.out.println("You have just Experienced Projectile Motion");
    }
    public static void GetValues() throws InterruptedException{
        Scanner GetAngle = new Scanner(System.in);

        System.out.println("Day and time: " + new Date());
        TimeUnit.SECONDS.sleep(1);
        System.out.println("Enter in the Required values");
        TimeUnit.SECONDS.sleep(1);
        System.out.println("Angle in degrees: > ");
        Double GivenAngle = GetAngle.nextDouble();
        TimeUnit.SECONDS.sleep(1);
        Scanner GetVelocity = new Scanner(System.in);
        System.out.println("Velocity in Meters/Second: > ");
        Double GivenVelocity = GetVelocity.nextDouble();
        TimeUnit.SECONDS.sleep(1);
        Scanner GetSteps = new Scanner(System.in);
        System.out.println("Number of Steps: > ");
        Double GivenSteps = GetSteps.nextDouble();
        TimeUnit.SECONDS.sleep(1);
        Double[] dooble = CalculatedValues(GivenAngle, GivenVelocity, GivenSteps);
        Content(dooble);
    }

    public static Double[] CalculatedValues(Double a, Double b, Double c){
        Double radiant = Math.toRadians(a);
        Double VertTravel = b * Math.sin(radiant);
        Double HorizonTravel = b * Math.cos(radiant);

        Double[] ValuesDone = {radiant, VertTravel, HorizonTravel, a, b, c};

        return ValuesDone;

    }

    public static void Content(Double[] x) throws InterruptedException{
        DrawingPanel Projectile = new DrawingPanel(1000, 1000);
        Graphics g = Projectile.getGraphics();
        g.fillOval(XPOS, YPOS, WIDTH, HEIGHT);
        TimeUnit.SECONDS.sleep(1);

        int xpos = XPOS;
        int ypos = YPOS;
        Double Gravity = GRAVITY;
        Double Seconds = SECONDS;
        Double NumOfSteps = x[5];
        Double VerticalVelocity = x[1];
        Double HorizontalVelocity = x[2];
        Double seconds = Seconds;

        while(Seconds < NumOfSteps){
            VerticalVelocity -= Gravity;
            xpos += 2*HorizontalVelocity;
            ypos += VerticalVelocity;
            g.fillOval(xpos, ypos, WIDTH, HEIGHT);
            Seconds++;
            TimeUnit.SECONDS.sleep(1);
            
        }

        while(seconds <= (NumOfSteps - 1)){
            VerticalVelocity -= Gravity;
            xpos += 2*HorizontalVelocity;
            g.fillOval(xpos, ypos, WIDTH, HEIGHT);
            seconds++;
            ypos -= VerticalVelocity;
            xpos+= 2*HorizontalVelocity;
            TimeUnit.SECONDS.sleep(1);
        }
    }
}